%% PROYECTO FINAL UNIDAD 2
clear all, clc, clear all, close all, warning off
%% APARTADO INTRODUCCIÓN A MEMORIAS
loadlibrary('smClient64.dll','./smClient.h')       %Cargar la librería
calllib('smClient64','createMemory','Datos',3,2)   %Crear memoria "Datos"
calllib('smClient64','createMemory','Visual',4,2)  %Crear memoria "Visual"
calllib('smClient64','openMemory','Datos',2)       %Abrir memoria "Datos"
calllib('smClient64','openMemory','Visual',2)      %Abrir memoria "Visual"
%% sistema de 1 tanque 
%% CONDICIONES INICIALES DE LOS TANQUES
h(1)=0;               %Altura del tanque inicial NO LIN.
h1(1)=0;              %Altura del tanque inicial LIN_1
tem(1)=0;             %Temperatura inicial del sistema
%% DATOS GENERALES SISTEMA 1
To=0.1;
Tfi=200;
t=[0:To:Tfi];
g=9.8;                  %Gravedad
k2=0.015;              %Constante de valvula 2
A=0.5;                 %Area de tanque 1
%% DATOS GENERALES SISTEMA 2
global dx 
%Valores iniciales
Q=2000;              %cuando hace cero la derivada (P.eq)
%Parametros
F=100;               %Intermitencia de agua
V=100;               %Volumen del tanque
rho=1;               %Densidad de agua (constante)
Cp=1;                %capacidad calorífica 
%% Punto de equilibrio de los tanques
a1e=0.36;            % Aber. val.1 en punto de equilibrio
a2e=0.3;             % Aber. val.2 en punto de equilibrio
%% Importación de Datos de Unity con memorias compartidas
 while true
 k1=calllib('smClient64','getFloat','Datos',0)
 T1=calllib('smClient64','getFloat','Datos',1)
 T=calllib('smClient64','getFloat','Datos',2)
%% Valores de apartura de valvulas a1,a2 
for k=1:length(t)
tic
% a1=[0.95*ones(1,200) 0.65*ones(1,200) 0.55*ones(1,200)...
%     0.45*ones(1,200) 0.40*ones(1,200) 0.70*ones(1,length(t))+1];
%     calllib('smClient64','setFloat','Visual',2,a1(k))
% a2=[0.65*ones(1,200) 0.50*ones(1,200) 0.60*ones(1,200)...
%     0.40*ones(1,200) 0.45*ones(1,200) 0.40*ones(1,length(t))+1];
%     calllib('smClient64','setFloat','Visual',3,a2(k))
a1=[0.90*ones(1,200) 0.60*ones(1,200) 0.55*ones(1,200)...
    0.60*ones(1,200) 0.60*ones(1,200) 0.70*ones(1,length(t))+1];
 calllib('smClient64','setFloat','Visual',2,a1(k))
a2=[0.65*ones(1,200) 0.50*ones(1,200) 0.40*ones(1,200)...
    0.45*ones(1,200) 0.80*ones(1,200) 0.40*ones(1,length(t))+1];
calllib('smClient64','setFloat','Visual',3,a2(k))
    %% MODELO NO LINEALIZADO SISTEMA 1
    hpNL=(k1*a1(k)-k2*a2(k)*sqrt(2*g*h(k)))/A;
    h(k+1)=hpNL*To+h(k);        
    % Punto de equilibrio para Tanque 1  
    he =(k1*a1e/(k2*a2e*sqrt(2*g)))^2;
    %% MODELO LINEALIZADO SISTEMA 1
    %Variaciones: Delta(a) y Delta(h) 
    da1(k)=a1(k)-a1e;
    da2(k)=a2(k)-a2e;
    dh2(k)=h1(k)-he;
    hp2=(k1*da1(k)-(k2*a2e*sqrt(2*g)/(sqrt(he)))*dh2(k)...
    -k2*sqrt(2*g*he)*da2(k))/A;
    h1(k+1)=hp2*To+h1(k);
    calllib('smClient64','setFloat','Visual',0,h1(k))
    %% MODELO NO LINEALIZADO SISTEMA 2
    dT=(F*a1(k))/V*(T1-T)+Q/(rho*V*Cp);
    Y(k+1)=dT*To+T;
    calllib('smClient64','setFloat','Visual',1,Y(k))
%Tiempo de muestreo
 while(toc < To)
end
dt(k)=toc;  %tiempo se guarda en una variable para luego representarse en grficas
end

%% Gráfica SISTEMA 1
figure(1)
w1=animatedline('color','r','Linewidth',1.5);      %no lineal 
w2=animatedline('color','g','Linewidth',1.5);      %lineal
w3=animatedline('color','b','Linewidth',1);        %a1k
w4=animatedline('color','c','Linewidth',1);        %a2k
w5=animatedline('color','m','Linewidth',.5);       %he
w6=animatedline('color','k','Linewidth',.5);       %a1e
w7=animatedline('color','y','Linewidth',.5);       %a2e
legend('MODELO NO LINEAL','MODELO LINEAL','a1(k)','a2(k)','he','a1e','a2e');
title('MODELAMEINTO TANQUE DE NIVEL CON TAYLOR');
xlabel('Tiempo [s]');
ylabel('Altura');
axis([0 100 -0.1 1.6])
grid on
for k=1:length(t)
    addpoints(w1,t(k),h(k)); 
    addpoints(w2,t(k),h1(k));
    addpoints(w3,t(k),a1(k)); 
    addpoints(w4,t(k),a2(k));
    addpoints(w5,t(k),he);
    addpoints(w6,t(k),a1e);
    addpoints(w7,t(k),a2e);
    drawnow limitrate;
end
%% Grafica SISTEMA 2
figure(2)
w1=animatedline('color','r','Linewidth',1.5); %no lineal sistema 2
legend('TEMPERATURA');
title('SISTEMA DE CALENTADO DE AGUA');
xlabel('Tiempo [s]');
ylabel('TEMPERATURA EN °C');
axis([0 100 35 60])
grid on
for k=1:length(t)
    addpoints(w1,t(k),Y(k)); 
    drawnow limitrate;
end
 end
%% Liberar las memorias
calllib('smClient64','freeViews')
unloadlibrary smClient64
