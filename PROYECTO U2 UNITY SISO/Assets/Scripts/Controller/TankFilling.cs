﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankFilling : MonoBehaviour
{
    [SerializeField]
    private Vector3 Posc_Nivel;              //Toma posicion del vector water
    [SerializeField]                         //Aparece en el inspector
    private GameObject water;                //Asignacion y declaracion del objeto
    [SerializeField]                         //Aparece en el inspector
    private double velocity;                 //Velocidad con la que va a evoulucionar un sistema
    private double altura;                   //Declaro variable privada Volumen Nivel
    private double auxiliar;                 //Declaro variable para condicionar el ingreento del volumen
                                                  
    void Start()
    {
        Posc_Nivel = water.transform.localPosition;  //Vector3 de posicion de la tuberia, tomo las cordenadas del plano interno de agua
    }

    void Update()
    {
        Filling();                          //Invoco el private void Filling (Llenado)
    }

    private void Filling()
    {

        //Ascenso de Nivel 
        #region
        altura = Setting.setting._altura;                                                                              //Tomo el valor de la variable Volumen del tanque de Nivel
        Debug.Log("ALTURA" + altura);                                                                                  //leyendo el valor de volumen

        if (Posc_Nivel.y < 0)                                                                                          //Pregunto la posision de PN es < 0 en la cordenada Y
        {
            auxiliar = -altura;                                                                                        //Cumple la condición, entonces decrementa el vol_nivel
        }
        else if (Posc_Nivel.y > 0)                                                                                     //Pregunto la posision de PN es > 0 en la cordenada Y
        {
            auxiliar = +altura;                                                                                        //Cumple la condición, entonces incrementa el vol_nivel
        }
        float scaley = System.Convert.ToSingle(Posc_Nivel.z + Setting.setting._altura/2* velocity);                    //variable local ojo con float hay que transformas
        water.transform.localScale = new Vector3(water.transform.localScale.x, water.transform.localScale.y, scaley);  //afectando la escala del objeto para que se produzca un 
        #endregion
    }
}
