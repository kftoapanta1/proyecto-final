﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;                                                                    //Libreria de dependencia de gameObject
using System.Runtime.InteropServices;                                                    //Libreria para Dll (Memorias ARSI)
using TMPro;                                                                             //Libreria para Lable (textos del Ins Virtual)

public class RunOperation : MonoBehaviour
{
    //private Memoryshare memoryshare;
    [SerializeField]                                                                      //Objeto visible en el inspector
    private Slider insp;                                                                  //Slider que responde a la variación d caudal
    public Slider _insp { set { this.insp = value; } get { return insp; } }               //Tomar el valor del slider
    private float k1;                                                                     //Variable flotante para el caudal de entrada
    [SerializeField]                                                                      //Objeto visible en el inspector
    private float containerVolume;                                                        //Variable flotante para la capacidad de volumen del tanque
    [SerializeField]                                                                      //Objeto visible en el inspector
    private float TAM;                                                                    //Variable flotante para la temperatura ambiente
    [SerializeField]                                                                      //Objeto visible en el inspector
    private float TDS;                                                                    //Variable flotante para la temperatura de salida
    [SerializeField]                                                                      //Objeto visible en el inspector                                                                    //Objeto visible en el inspector
    private float a1;                                                                     //Variable flotante para la apertura de la válvula de entrada
    [SerializeField]                                                                      //Objeto visible en el inspector
    private float a2;                                                                     //Variable flotante para la apertura de válvula de salida
    [SerializeField]                                                                      //Objeto visible en el inspector
    private float altura;                                                                 //Variable flotante para la altura del tanque de nivel
    [SerializeField]                                                                      //Objeto visible en el inspector
    private float h;                                                                      //Variable flotante para la altura de integración
    [SerializeField]                                                                      //Objeto visible en el inspector
    private float temperatura;                                                            //Variable flotante para el volumen del tanque de nivel
    [SerializeField]                                                                      //Objeto visible en el inspector
    private TMP_Text[] label;                                                             //Vector para label
    private float A;                                                                      //Variable flotante para el área
    private float k2;                                                                     //Variable flotante para la constante de vlvula de salida
    private float g;                                                                      //Variable flotante para gravedad
    private float hp;                                                                     //Variable flotante para hp
    private float time;                                                                   //Variable flotante time
    private float TimeSample;                                                             //Variable flotante Time Sample
    float To;                                                                             //Variable flotante To

    #region ImportacionDll
    const string dllPath = "smClient64.dll";                                              //Llamado a la libreia smClient64.dll
                                                                                          //Crear la memoria: Este paso se lo realizó en Matlab
    [DllImport(dllPath)]                                                                  //Acciones rapidas y refactorizantes, importar el Patch
    public static extern int openMemory(String name, int typeCode);                       //Definir el posterior uso de abrir la memoria compartida ARSI       
    [DllImport(dllPath)]                                                                  //Importar Patch
    public static extern void setFloat(String memName, int position, float fValue);       //Definir el post uso de escribir en la memorias ARSI
    [DllImport(dllPath)]                                                                  //Importar Patch
    public static extern float getFloat(String memName, int position);                    //Definir el post uso de leer en las memorias ARSI
                                                           
     #endregion
    void Start() 
    {
        for (int i = 0; i < label.Length; i++)                                            //For para incremento desde 0 con pasos de 1
        {
            label[i].text = System.Convert.ToString(0);                                   //Label de texto toma un valor numerico para mostrarlo en el display
        }
                                                                                          //Variable tf toma el valor de 50
        openMemory("Datos", 2);                                                           //Abrir las memoria "Datos" creada en MatLab, tipo frotante (2)
        openMemory("Visual", 2);                                                          //Abrir memoria "Visual" creada en MatLab, tipo flotante (2)
        k2 = 0.015f;                                                                      //Constante de la valvula de descarga                                                
        g = 9.8f;                                                                         //[m/s^2] valor de la gravedad en el planeta tierra
        h = 0f;                                                                           //[m] altura inicial para el modelo no lineal 
        A = 0.5f;                                                                         //[m^2] Área de la base del tanque 
        TimeSample = 0.1f;                                                                //[s] Definir variable para la sincronizacion de Unity y MatLab
        To = 0;                                                                           //[s] Tiempo de finalización      
    }

    void Update()
    {
                                                                                          //Void trabaja 1/frame=1/60fps= 16ms y MatLab trabaja a 100 ms hay sincronizarlos
        To += Time.deltaTime;                                                             //Cada 16ms se le suma el valor To
        if (To > TimeSample)                                                              //Cada que supera los 0.1s
        {
            modelResult();                                                                //Invocar heigResult
            Debug.Log(To);                                                                //Se observa en la consola de Unity que el muestreo es cada 100ms =100ms de matlab
            To = 0;                                                                       //Serializa en 0  
        }
    }
    private void modelResult()
    {
            k1= System.Convert.ToSingle(insp.value);                                     //Se asigna el valor del slider a la variable k1
            label[0].text = System.Convert.ToString(System.Math.Round(k1, 3));           //Asigno el valor de k1 a label [0] redondeado a 2 decimales  
            Setting.setting._k1 = k1;                                                    //Actualizo el valor de k1 a la clase setting, para que actualice constantemente del slider
            setFloat("Datos", 0, k1);                                                    //Escribo en la memoria "Datos" en la poscion 0 el valor de k1

            label[2].text = System.Convert.ToString(System.Math.Round(TAM, 2));          //Asigno el valor de TAM a label [1] redondeado a 2 decimales
            Setting.setting._TAM = TAM;                                                  //Actualizo el valor a la clase settinG, actualización costante
            setFloat("Datos", 1, TAM);                                                   //Escribo en la memoria "Datos" en la poscion 0 el valor de TAM

            label[3].text = System.Convert.ToString(System.Math.Round(TDS, 2));          //Asigno el valor de TDS a label [2] redondeado a 2 decimales 
            Setting.setting._TDS = TDS;                                                  //Actualizo el valor a la clase setting, actualización costante
            setFloat("Datos", 2, TDS);                                                   //Escribo en la memoria "Datos" en la poscion 0 el valor de TDS

            altura = getFloat("Visual", 0);                                              //Leo de la  memoria compartida "Visual" desdes MATLAB la posicion 0
            Setting.setting._altura = altura;                                            //Actualizo el valor de altura a clase setting   
            label[4].text = System.Convert.ToString(System.Math.Round(altura, 4));       //Asigno el valor de altura a label [2] redondeado a 2 decimales

            temperatura = getFloat("Visual", 1);                                         //Leo de la  memoria compartida "Caudal" desdes MATLAB la posicion 1
            Setting.setting._temperatura = temperatura;                                  //Actualizo el valor de temperaura a la clase setting 
            label[5].text = System.Convert.ToString(System.Math.Round(temperatura, 2));  //Asigno el valor de cdl_out a label [1] redondeado a 2 decimales

            a1 = getFloat("Visual", 2);                                                  //Leo de la  memoria compartida "Vál" desdes MATLAB la posicion 0
            Setting.setting._a1 = a1;                                                    //Leo de la  memoria compartida "Vïsual" desdes MATLAB la posicion 2

            a2 = getFloat("Visual", 3);                                                  //Leo de la  memoria compartida "Vïsual" desdes MATLAB la posicion 2
            Setting.setting._a2 = a2;                                                    //Actualizo el valor de de apertura de la válvula 2 en la clase setting 

                                           //***********MODELO MATEMÁTICO******//  
            hp = Convert.ToSingle((a1 * k1) - (a2 * k2 * Math.Sqrt(2 * g * h))) / A;
            h = hp * To + h;                                                             //Integración Numérica
            Setting.setting._h = h;                                                      //Actualizo el valor de de apertura de la válvula 2 en la clase setting
            label[1].text = System.Convert.ToString(System.Math.Round(h, 4));            //Asigno el valor de cdl_int a label [0] redondeado a 2 decimales

    }

}

