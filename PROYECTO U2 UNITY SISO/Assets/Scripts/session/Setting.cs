﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Setting : MonoBehaviour
{
    public static Setting setting;                                                                             //Requiero que mnatenga la informacion siendo clase statica y acceder por atributos
    //Forma 1: lenguaje de sintaxis
    private double containerVolume;                                                                            //Solo aqui cambio el valor del volumen del tanque
    public double _containerVolume { set { this.containerVolume = value; } get { return containerVolume; } }   //Metodo de escribir y leer el valor de containerValue
    private double k1;                                                                                         //Definir una varible pusblica tipo flotante para caudal de entrada
    public double _k1 { set { this.k1 = value; } get { return k1; } }                                          //Metodo de escribir y leer el caudal de entrada calculado mediante codigo
    private double TAM;                                                                                        //Definir una varible pusblica tipo flotante para caudal de salida
    public double _TAM{ set { this.TAM = value; } get { return TAM; } }                                        //Metodo de escribir y leer el caudal de salida calculado mediante codigo
    private double TDS;                                                                                        //Definir una varible pusblica tipo flotante para caudal de salida
    public double _TDS { set { this.TDS = value; } get { return TDS; } }                                       //Metodo de escribir y leer el caudal de salida calculado mediante codigo
    private double altura;                                                                                     //Definir una varible pusblica tipo flotante para altura
    public double _altura { set { this.altura = value; } get { return altura; } }                              //Metodo de escribir y leer la altura calculado mediante codigo
    private double temperatura;                                                                                //Definir una varible pusblica tipo flotante para volumen
    public double _temperatura { set { this.temperatura = value; } get { return temperatura; } }               //Metodo de escribir y leer la temperatura de Matlab
    private double a1;                                                                                         //Definir una varible pusblica tipo flotante para a1
    public double _a1 { set { this.a1 = value; } get { return a1; } }                                          //Metodo de escribir y leer la temperatura de Matlab
    private double a2;                                                                                         //Definir una varible pusblica tipo flotante para a2
    public double _a2 { set { this.a2 = value; } get { return a2; } }                                          //Metodo de escribir y leer la temperatura de Matlab
    private double h;                                                                                         //Definir una varible pusblica tipo flotante para a2
    public double _h { set { this.h= value; } get { return h; } }


    private void Awake()
    {
                                                   //Inicializa antes de star
        if (Setting.setting == null)               //Verifica que el objeto se encuentre en nulo
        {
            Setting.setting = this;                //Se asigna el obj seting y this apunta a si mismo
        }
        else
        {
            if (Setting.setting != this)           //Pregunta si la clase setting es diferente de 0
            {
                Destroy(this.gameObject);          //Destruye el objeto
            }
        }
        DontDestroyOnLoad(this.gameObject);        //Cierra el programa de unity cuento existe alguna anomalía con containerVolume
        Debug.Log(containerVolume);                //Da un mensaje en la consola de Unity
    }
}